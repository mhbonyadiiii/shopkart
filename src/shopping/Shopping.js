import React, { useContext } from "react";
import "./shopping.css";
import Shopkart from "../shopkart/shopkart";
import { CartContext } from "../Context/Context";

const Shopping = () => {
  const { active, setActive, ent, setEnt } = useContext(CartContext);

  const Data = [
    {
      start: "10",
      end: "11",
      value: 20,
      price: "100",
    },
    {
      start: "12",
      end: "13",
      value: 20,
      price: "100",
    },
    {
      start: "13",
      end: "14",
      value: 20,
      price: "100",
    },
    {
      start: "15",
      end: "16",
      value: 20,
      price: "100",
    },
    {
      start: "17",
      end: "18",
      value: 20,
      price: "100",
    },
    {
      start: "19",
      end: "20",
      value: 20,
      price: "100",
    },
    {
      start: "21",
      end: "22",
      value: 20,
      price: "100",
    },

    {
      start: "23",
      end: "24",
      value: 20,
      price: "100",
    },
    {
      start: "25",
      end: "26",
      value: 20,
      price: "100",
    },
    {
      start: "27",
      end: "28",
      value: 20,
      price: "100",
    },
    {
      start: "29",
      end: "30",
      value: 20,
      price: "100",
    },
    {
      start: "31",
      end: "32",
      value: 20,
      price: "100",
    },
    {
      start: "33",
      end: "34",
      value: 20,
      price: "100",
    },
    {
      start: "35",
      end: "36",
      value: 20,
      price: "100",
    },
  ];
  const Entertainments = ["غواصی", "بنانا", "کایاک", "پدال"];

  return (
    <div className="container">
      <div
        style={{
          height: 200,
          width: "70vw",
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
          direction: "rtl",
          cursor: "pointer",
        }}
      >
        {Entertainments.map((item, indexi) => (
          <div
            style={
              indexi == active
                ? {
                    height: 55,
                    width: 110,
                    backgroundColor: "black",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }
                : {
                    height: 55,
                    width: 110,
                    backgroundColor: "grey",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }
            }
            onClick={() => {
              setActive(indexi);
              setEnt(item)
            }}
          >
            <h2 style={{ color: "white", userSelect: "none" }}>{item}</h2>
          </div>
        ))}
      </div>
      <div className="cardPaage">
        {Data.map((item, index) => (
          <Shopkart
            Start={item.start}
            End={item.end}
            Capacity={item.value}
            Price={item.price}
            Entertainment={ent}
          />
        ))}
      </div>
    </div>
  );
};

export default Shopping;
