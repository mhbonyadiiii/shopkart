import React, { useContext } from "react";
import { CartContext } from "../Context/Context";

const Basket = () => {
  const {Data, setData} = useContext(CartContext);
  return (
    <div
      style={{
        height: 'fitContent',
        width: "100vw",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      {Data.map((item, index) => (
        <div
          style={{
            height: 200,
            width: 700,
            backgroundColor: "black",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "space-around",
            marginTop : 20,
            marginBottom : 10,
            color : 'white',
            borderRadius : 10,
            
          }}
        >
          <p>ساعت سانس :{item.time}</p>
          <p>تعداد :{item.num}</p>
          <p>قیمت هر بلیت :{item.Price}</p>
        </div>
      ))}
    </div>
  );
};

export default Basket;
