import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import Shopping from "./shopping/Shopping"
import Basket from "./basket/basket";
import { Provider } from "./Context/Context";

function App() {
  return (
    <Provider>
      <div style={{overflowX : 'hidden'}}>
      <Router>
        <div className="hedear">
          <button
            style={{
              backgroundColor: "black",
              color: "white",
              width: "6vw",
              height: "100%",
              fontSize: 24,
              fontWeight: "bold",
              outline: "none",
              border: "none",
            }}
          >
            <Link
              style={{ textDecoration: "none", color: "white" }}
              to="/basket"
            >
              سبد خرید
            </Link>
          </button>
          <button
            style={{
              backgroundColor: "black",
              color: "white",
              width: "6vw",
              height: "100%",
              fontSize: 24,
              fontWeight: "bold",
              outline: "none",
              border: "none",
            }}
          >
            <Link style={{ textDecoration: "none", color: "white" }} to="/">
              صفحه خرید
            </Link>
          </button>
        </div>
        <Switch>
          <Route exact path="/" component={Shopping} />
          <Route path="/basket" component={Basket}/>
        </Switch>
      </Router>
      </div>
    </Provider>
  );
}

export default App;
