import React from 'react'

const Counter = ({ State, Add, Remove }) => {
    return (
      <div
        style={{
          width: "60%",
          display: "flex",
          justifyContent: "space-around",
          alignItems: "center",
        }}
      >
        <button onClick={Add} style={{ width: 30, height: 30 }}>
          +
        </button>
        <p>{State}</p>
        <button onClick={Remove} style={{ width: 30, height: 30 }}>
          -
        </button>
      </div>
    );
  };

export default Counter
