import React, { useContext, useState } from "react";
import { CartContext } from "../Context/Context";
import "./shopkard.css";
import Counter from "../Counter/Counter";

export default function Shopkart({
  Start,
  End,
  Price,
  Capacity,
  Entertainment,
}) {
  const {Data, setData} = useContext(CartContext);
  const [counter, setcounter] = useState(0);
  const [shop, setshop] = useState(false);
  const [All, setAll] = useState(Capacity);
  const [WomanTicket, setWomanTicket] = useState(0);
  const [ManTicket, setManTicket] = useState(0);
  let leftValue = All;
  let both = ManTicket + WomanTicket;

  const Shop = () => {
    setshop(true);
  };

  const Progress = () => {
    if (Entertainment === "غواصی") {
      setAll(All - both);
      setNum();
      setWomanTicket(0)
      setManTicket(0)
    } else {
      if (counter <= leftValue) {
        setAll(All - counter);
        setcounter(0);
        setNum();
      }
    }
  };

  const setNum = () => {
    if (Entertainment === "غواصی") {
      let temp = Data;
      temp.push({
        time: Start,
        Price: Price,
        num: both,
      });
      setData(temp);
    } else {
      let temp = Data;
      temp.push({
        time: Start,
        Price: Price,
        num: counter,
      });
      setData(temp);
    }
  };

  const Ziyad = () => {
    if (counter < leftValue) {
      setcounter(counter + 1);
    }
  };
  const kam = () => {
    if (counter > 0) {
      setcounter(counter - 1);
    }
  };

  const addMan = () => {
    if (both < leftValue) {
      setManTicket(ManTicket + 1);
    }
  };
  const removeMan = () => {
    if (ManTicket > 0) {
      setManTicket(ManTicket - 1);
    }
  };

  const addWoman = () => {
    if (both < leftValue) {
      setWomanTicket(WomanTicket + 1);
    }
  };
  const removeWoman = () => {
    if (WomanTicket > 0) {
      setWomanTicket(WomanTicket - 1);
    }
  };

  return (
    <div className="card">
      <p>{Entertainment}</p>
      <p>شروع سانس:{Start}</p>
      <p>پایان سانس :{End}</p>
      <div className="border"></div>
      <p>ظرفیت باقی مانده :{leftValue}</p>

      <p>{Price} تومان </p>
      {shop == false ? (
        <button className="buyButtton" onClick={Shop}>
          انتخاب کنید{" "}
        </button>
      ) : Entertainment === "غواصی" ? (
        <>
          <Counter State={ManTicket} Add={addMan} Remove={removeMan} />
          <Counter State={WomanTicket} Add={addWoman} Remove={removeWoman} />
          <button onClick={Progress} className="buyButtton">
            انتخاب کنید{" "}
          </button>
        </>
      ) : (
        <>
          <Counter State={counter} Add={Ziyad} Remove={kam} />
          <button onClick={Progress} className="buyButtton">
            انتخاب کنید{" "}
          </button>
        </>
      )}
    </div>
  );
}
