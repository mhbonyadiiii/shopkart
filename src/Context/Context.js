import React, { createContext, useState } from "react";

export const CartContext = createContext();

export const Provider = ({ children }) => {
  const [Data, setData] = useState([]);
  const [active, setActive] = useState(0);
  const [ent,setEnt] = useState('غواصی')

  return (
    <CartContext.Provider value={{ Data, setData, active, setActive,ent,setEnt }}>
      {children}
    </CartContext.Provider>
  );
};
